var abol = new Arbol();
var buscarNd = [];
this.buscarNodos = [];

abol.NodoPadre.Ni = abol.agregarNodo(16, 16, abol.NodoPadre, abol.NodoPadre.nivel);

abol.NodoPadre.Ni.Ni = abol.agregarNodo(8, 8, abol.NodoPadre.Ni, abol.NodoPadre.Ni.nivel);

abol.NodoPadre.Ni.Ni.Ni = abol.agregarNodo('e', 4, abol.NodoPadre.Ni.Ni, abol.NodoPadre.Ni.Ni.nivel);
abol.NodoPadre.Ni.Ni.Nd = abol.agregarNodo(4, 4, abol.NodoPadre.Ni.Ni, abol.NodoPadre.Ni.Ni.nivel);

abol.NodoPadre.Ni.Ni.Nd.Ni = abol.agregarNodo('n', 2, abol.NodoPadre.Ni.Ni.Nd, abol.NodoPadre.Ni.Ni.Nd.nivel);
abol.NodoPadre.Ni.Ni.Nd.Nd = abol.agregarNodo(2, 2, abol.NodoPadre.Ni.Ni.Nd, abol.NodoPadre.Ni.Ni.Nd.nivel);

abol.NodoPadre.Ni.Ni.Nd.Nd.Ni = abol.agregarNodo('o', 1, abol.NodoPadre.Ni.Ni.Nd.Nd, abol.NodoPadre.Ni.Ni.Nd.Nd.nivel);
abol.NodoPadre.Ni.Ni.Nd.Nd.Nd = abol.agregarNodo('u', 1, abol.NodoPadre.Ni.Ni.Nd.Nd, abol.NodoPadre.Ni.Ni.Nd.Nd.nivel);

abol.NodoPadre.Ni.Nd = abol.agregarNodo(8, 8, abol.NodoPadre.Ni, abol.NodoPadre.Ni.nivel);

abol.NodoPadre.Ni.Nd.Ni = abol.agregarNodo('a', 4, abol.NodoPadre.Ni.Nd, abol.NodoPadre.Ni.Nd.nivel);
abol.NodoPadre.Ni.Nd.Nd = abol.agregarNodo(4, 4, abol.NodoPadre.Ni.Nd, abol.NodoPadre.Ni.Nd.nivel);

abol.NodoPadre.Ni.Nd.Nd.Ni = abol.agregarNodo('t', 2, abol.NodoPadre.Ni.Nd.Nd, abol.NodoPadre.Ni.Nd.Nd.nivel);
abol.NodoPadre.Ni.Nd.Nd.Nd = abol.agregarNodo('m', 2, abol.NodoPadre.Ni.Nd.Nd, abol.NodoPadre.Ni.Nd.Nd.nivel);

//Parte derecha
abol.NodoPadre.Nd = abol.agregarNodo(20, 20, abol.NodoPadre, abol.NodoPadre.nivel);

abol.NodoPadre.Nd.Ni = abol.agregarNodo(8, 8, abol.NodoPadre.Nd, abol.NodoPadre.Nd.nivel);

abol.NodoPadre.Nd.Ni.Ni = abol.agregarNodo(4, 4, abol.NodoPadre.Nd.Ni, abol.NodoPadre.Nd.Ni.nivel);

abol.NodoPadre.Nd.Ni.Ni.Ni = abol.agregarNodo('i', 2, abol.NodoPadre.Nd.Ni.Ni, abol.NodoPadre.Nd.Ni.Ni.nivel);
abol.NodoPadre.Nd.Ni.Ni.Nd = abol.agregarNodo(2, 2, abol.NodoPadre.Nd.Ni.Ni, abol.NodoPadre.Nd.Ni.Ni.nivel);

abol.NodoPadre.Nd.Ni.Ni.Nd.Ni = abol.agregarNodo('x', 1, abol.NodoPadre.Nd.Ni.Ni.Nd, abol.NodoPadre.Nd.Ni.Ni.Nd.nivel);
abol.NodoPadre.Nd.Ni.Ni.Nd.Nd = abol.agregarNodo('p', 1, abol.NodoPadre.Nd.Ni.Ni.Nd, abol.NodoPadre.Nd.Ni.Ni.Nd.nivel);

abol.NodoPadre.Nd.Ni.Nd = abol.agregarNodo(4, 4, abol.NodoPadre.Nd.Ni, abol.NodoPadre.Nd.Ni.nivel);

abol.NodoPadre.Nd.Ni.Nd.Ni = abol.agregarNodo('h', 2, abol.NodoPadre.Nd.Ni.Nd, abol.NodoPadre.Nd.Ni.Nd.nivel);
abol.NodoPadre.Nd.Ni.Nd.Nd = abol.agregarNodo('s', 2, abol.NodoPadre.Nd.Ni.Nd, abol.NodoPadre.Nd.Ni.Nd.nivel);

abol.NodoPadre.Nd.Nd = abol.agregarNodo(12, 12, abol.NodoPadre.Nd, abol.NodoPadre.Nd.nivel);

abol.NodoPadre.Nd.Nd.Ni = abol.agregarNodo(5, 5, abol.NodoPadre.Nd.Nd, abol.NodoPadre.Nd.Nd.nivel);

abol.NodoPadre.Nd.Nd.Ni.Ni = abol.agregarNodo(2, 2, abol.NodoPadre.Nd.Nd.Ni, abol.NodoPadre.Nd.Nd.Ni.nivel);

abol.NodoPadre.Nd.Nd.Ni.Ni.Ni = abol.agregarNodo('r', 1, abol.NodoPadre.Nd.Nd.Ni.Ni, abol.NodoPadre.Nd.Nd.Ni.Ni.nivel);
abol.NodoPadre.Nd.Nd.Ni.Ni.Ni = abol.agregarNodo('l', 1, abol.NodoPadre.Nd.Nd.Ni.Ni, abol.NodoPadre.Nd.Nd.Ni.Ni.nivel);

abol.NodoPadre.Nd.Nd.Ni.Nd = abol.agregarNodo('f', 3, abol.NodoPadre.Nd.Nd.Ni, abol.NodoPadre.Ni.nivel);

abol.NodoPadre.Nd.Nd.Nd = abol.agregarNodo(' ', 7, abol.NodoPadre.Nd.Nd, abol.NodoPadre.Nd.Nd.nivel);


var a = abol.VHijos(abol.NodoPadre);
console.log(a);

var z =  abol.bValor(abol.nodoPadre);
console.log(z);

var caminoarbol = abol.bCamino(z);
console.log(caminoarbol);

var sum = abol.SumarCaminoN(z);
console.log(sum);