class Nodo{
    constructor(nodoPadre , posicion ,nombre, valor, nivel) {
        this.nombre = nombre;
        this.padre = nodoPadre;
        this.posicion = posicion;
        this.valor = valor;
        this.nivel = nivel;
    }
}