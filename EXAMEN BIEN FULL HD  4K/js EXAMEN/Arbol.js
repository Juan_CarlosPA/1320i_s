class Arbol{
    constructor(){
        this.nodoPadre = this.NodoPadre();
        this.bElemento = 4;
        this.bNodos = [];
        this.nivel = 3;
        this.cam = '';
        this.sumar = 0;
    }
    agregarNodo(nombre, valor, padre, nivel){
        var nodo = new Nodo(nombre, valor, padre, nivel);
        return nodo;
    }

    NodoPadre(nombre = 36, valor = 36, padre = null, nivel = null){
        var nodo = new Nodo(nombre, valor, padre, nivel);
        return nodo;
    }

    VHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.bNodos.push(nodo.valor);

        if(nodo.hasOwnProperty('Nd'))
            this.VHijos(nodo.Nd);

        if(nodo.hasOwnProperty('Ni'))
            this.VHijos(nodo.Ni);


        return this.bNodos;

    }

    bValor(nodo){

        if(nodo.valor == this.bElemento)
            this.bElemento = nodo;

        if(nodo.hasOwnProperty('hijoI'))
            this.bValor(nodo.hijoI);

        if(nodo.hasOwnProperty('hijoD'))
            this.bValor(nodo.hijoD);

        return this.bElemento;
    }

    bCamino(nodo){

        if(nodo.padre != null){
            this.cam = this.cam + ' '+ nodo.padre.valor;
            this.bCamino(nodo.padre);
        }

        return this.bElemento.valor + ' ' + this.cam;
    }
    SumarCaminoN(nodo){
        
        
        if(nodo.padre != null){
            this.sumar = this.sumar + nodo.padre.valor;
            this.SumarCaminoN(nodo.padre);
           
        }
        return this.bElemento.valor + this.sumar;
    }

}

