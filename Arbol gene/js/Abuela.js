class Abuela{
    constructor(){
        this.Cabello = "Lacio";
        this.ColorOjos = "Cafe claro";
        this.Piel = "Morena";
        this.Altura = 1.52;
    }

    get CareteristicasAbuela(){
        return "Caracteristicas de la abuela" + " \n" + this.Cabello + " \n" + this.ColorOjos + " \n" + this.Piel + 
        " \n" + this.Altura;
    }

}