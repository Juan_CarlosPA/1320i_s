class Abuelo{
    constructor(){
        this.Cabello = "Lacio";
        this.ColorOjos = "Cafe oscuro";
        this.Piel = "Blanca";
        this.Altura = 1.70;
    }

    get CareteristicasAbuelo(){
        return "Caracteristicas del abuelo" + " \n" + this.Cabello + " \n" + this.ColorOjos + " \n" + this.Piel + 
        " \n" + this.Altura;
    }

}