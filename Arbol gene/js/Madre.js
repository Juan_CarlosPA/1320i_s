class Madre extends Abuela{
    constructor(){
        super();
        this.Conflexion = "Robusta";
        this.ApPaterno = "Aragon";
        this.Nombre = "Maria de Lourdes";
    }

    get CareteristicasMadre(){
        return "Caracteriticas de la madre:" + " \n" + this.Conflexion + " \n" + this.ApPaterno + " \n"+ this.Nombre;
    }
}