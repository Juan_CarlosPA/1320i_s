class Padre extends Abuelo{
    constructor(){
        super();
        this.Conflexion = "Delgada";
        this.ApPaterno = "Palma";
        this.Nombre = "Martin";
    }

    get CareteristicasPadre(){
        return "caracteriticas del padre:" + " \n" + this.Conflexion + " \n" + this.ApPaterno + " \n"+ this.Nombre;
    }
}