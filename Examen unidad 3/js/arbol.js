class Arbol{

    constructor() {
        this.nodoPadre = "";
        this.nodos = [];
        
    }

    agregarNodoPadre(nodoPadre,posicion,nombre,valor,nivel){
        var nodo = new Nodo(null,null,nombre,valor,nivel);
        this.nodos.push(nodo);
        return nodo;
    }

    agregarNodo(nodoPadre,posicion,nombre,valor,nivel){
        var nodo = new Nodo(nodoPadre,posicion,nombre,valor,nivel);
        this.nodos.push(nodo);
        return nodo;
    }

    agregarNodoNuevo(valor, nombre){
        let nodo = new Nodo(null, null, nombre, valor, null);
        if(this.nodos.length == 1){
            nodo.padre = this.nodoPadre;
            nodo.nivel > this.nodoPadre.nivel +1;
            if(nodo.valor < this.nodoPadre.valor){
                //--Izquierda
                nodo.posicion = 'HI';
            }else{
                nodo.posicion = 'HD';
            }
        }
        this.nodos.push(nodo);
    }

}