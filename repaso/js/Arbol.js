class Arbol{
    constructor(){
        this.nodoPadre = "";
        //this.bElemento;
        //this.bNodos = [];
        //this.nivel = 2;
       // this.cam = '';
        //this.sumar = 0;
    }
   

    NodoPadre(nombre, valor, padre, nivel){
        var nodo = new Nodo(nombre, valor, padre, nivel);
        return nodo;
    }

     agregarNodo(nombre, valor, padre, nivel){
        var nodo = new Nodo(nombre, valor, padre, nivel);
        return nodo;
    }



    VHijos(nodo){

        if(nodo.nivel == this.nivel)
            this.bNodos.push(nodo.valor);

        if(nodo.hasOwnProperty('Ni'))
            this.VHijos(nodo.Ni);

        if(nodo.hasOwnProperty('Nd'))
            this.VHijos(nodo.Nd);


        return this.buscarNodos;

    }

    bValor(nodo){

        if(nodo.valor == this.bElemento)
            this.bElemento = nodo;

        if(nodo.hasOwnProperty('Ni'))
            this.bValor(nodo.Ni);

        if(nodo.hasOwnProperty('Nd'))
            this.bValor(nodo.Nd);

        return this.bElemento;
    }

    bCamino(nodo){

        if(nodo.padre != null){
            this.cam = this.cam + ' '+ nodo.padre.valor;
            this.bCamino(nodo.padre);
        }

        return this.bElemento.valor + ' ' + this.cam;
    }
    SumarCaminoN(nodo){
        
        
        if(nodo.padre != null){
            this.sumar = this.sumar + nodo.padre.valor;
            this.SumarCaminoN(nodo.padre);
           
        }
        return this.bElemento.valor + this.sumar;
    }

}

